SHELL := /bin/bash

# 'make freshinstall' (or just 'make') prepares and starts a dockerized instance of Codex
# 	A phabricator ticket may also be specified, from which the latest patchset in Gerrit will be used:
# 		'make ticket=T294161'
#	A Gerrit change number and optional patchset number may also be used:
#		'make change=733098'
# 		or
# 		'make change=733098 patchset=32'
# 	The ticket argument will only work for tickets which have an accompanying Codex patch in Gerrit
.DEFAULT: freshinstall
.PHONY: freshinstall
freshinstall:
	make stop
	make remove
	make prepare
	make start

# 'make prepare' fetches the Codex repo and builds the docker container
.PHONY: prepare
prepare:
	-git clone https://gerrit.wikimedia.org/r/design/codex
	-@if [ "$(change)" ] && [ "$(patchset)" ]; then \
		cd "codex"; \
		eval $$(curl -sk \
			"https://gerrit.wikimedia.org/r/changes/$(change)/?o=ALL_REVISIONS&o=DOWNLOAD_COMMANDS" | \
			sed '1d' | \
			python -c \
			"import sys, json; \
			print(list(filter(lambda rev: rev['_number'] == $(patchset), list(json.load(sys.stdin)['revisions'].values())))[0]['fetch']['anonymous http']['commands']['Checkout']) \
			" \
		); \
	elif [ "$(change)" ] ; then \
		cd "codex"; \
		eval $$(curl -sk \
			"https://gerrit.wikimedia.org/r/changes/$(change)/?o=CURRENT_REVISION&o=DOWNLOAD_COMMANDS" | \
			sed '1d' | \
			python -c \
			"import sys, json; \
			print(list(json.load(sys.stdin)['revisions'].values())[0]['fetch']['anonymous http']['commands']['Checkout']) \
			" \
		); \
	elif [ "$(ticket)" ]; then \
		cd "codex"; \
		eval $$(curl -sk \
			"https://gerrit.wikimedia.org/r/changes/?q=bug:$(ticket)&o=CURRENT_REVISION&o=DOWNLOAD_COMMANDS" | \
			sed '1d' | \
			python -c \
			"import sys, json; \
			print(list(json.load(sys.stdin)[0]['revisions'].values())[0]['fetch']['anonymous http']['commands']['Checkout']) \
			" \
		); \
	fi
	./apply-patches.sh
	docker compose up --no-start

# 'make remove' removes the fetched Codex repo code and docker container
.PHONY: remove
remove:
	-docker container rm codex-docker
	-docker image rm codex-docker
	-@if [ -d "./codex" ]; then \
		read -p "Are you sure you wish to delete the existing Codex folder? (y/n)? " -n 1 -r; \
		echo ; \
		if [ "$$REPLY" = "y" ]; then \
			rm -rf ./codex; \
		fi \
	fi

# 'make start' starts (or creates and runs) the Codex docker container and opens the main demo page
.PHONY: start
start:
	( sleep 8; open http://localhost:3000 || xdg-open http://localhost:3000 || echo "Open 'http://localhost:3000' in a browser to view Codex doc pages." ) &
	docker compose up

# 'make stop' powers down the Codex docker container
.PHONY: stop
stop:
	-docker compose down

# 'make restart' restarts the Codex docker container and opens the main demo page
.PHONY: restart
restart:
	make stop
	make start

# 'make bash' is a quick way to access a bash shell in the container
.PHONY: bash
bash:
	docker exec -it codex-docker /bin/bash

# 'make test' runs Codex tests
.PHONY: test
test:
	docker exec -t codex-docker /bin/bash -c "npm run test"

# 'make snapshot' runs Codex tests updating snapshots
.PHONY: snapshot
snapshot:
	docker exec -t codex-docker /bin/bash -c "npm run test -- -u"