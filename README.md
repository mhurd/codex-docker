Quickly spin up the [Codex](https://phabricator.wikimedia.org/project/view/5587/) doc pages.

## Installation 

Clone the repo:

    git clone https://gitlab.wikimedia.org/mhurd/codex-docker.git

## Usage

Switch to the `codex-docker` directory:

    cd ~/codex-docker

Now you can spin up the Codex doc pages with any of the following commands:

-   ```
    make
     ```
    Make fetches the latest Codex repo (into `~/codex-docker/codex/`) and spins up a Docker container using it
-   ```
    make ticket=T294161
    ```
    Entering a ticket number along with *make* will fetch and spin up Codex with the latest Gerrit patchset associated with that ticket. *Note: the ticket argument will only work for tickets which have an accompanying Codex patch in Gerrit*
     
-   ```
    make change=733098
    ```
    Entering a Gerrit change number along with *make* will fetch and spin up Codex with the latest patchset associated with that change
     
-   ```
    make change=733098 patchset=32
     ```
    Entering a Gerrit change and patchset number along with *make* will fetch and spin up Codex with that specific patchset for that change

You can run these test-related *make* commands in a new terminal window **after** spinning up Codex with one of the other *make* commands:

-   ```
    make test
    ```
    Runs Codex tests
     
-   ```
    make snapshot
    ```
    Runs Codex tests updating snapshots