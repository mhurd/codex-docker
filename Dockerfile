FROM node:12.21.0

WORKDIR /usr/src/codex

COPY codex /usr/src/codex

RUN npm install -g npm@latest && \
    npm install --save-dev @wikimedia/codex @wikimedia/codex-icons